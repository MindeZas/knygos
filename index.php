<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Knygos</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="main-content">
		<div class="linkai">
			<ul>
				<li class="current" ><a href="index.php">UŽSAKYMO FORMA </a></li>
				<li><a href="uzsakymai.php"> UŽSAKYMAI </a></li>
			</ul>
		</div>
		<div class="forma">
		<h3><b>KNYGOS UŽSAKYMO FORMA</b></h3>
	<?php
/***********************************************/
include('connection.inc.php');
/***********************************************/
/******************puslapaivimo init***********************/
	$per_page=10;
	if (isset($_GET["page"])) {
	$page = $_GET["page"];
	}
	else {
	$page=1;
	}
/********************************************************/
/**********duomenų suvedimas********************/
$title = $year = $author = $genre = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if(isset($_POST["title"])||(isset($_POST["year"]))||(isset($_POST["author"]))||(isset($_POST["genre"]))){
		$k=true;
		if((strlen(test_input($_POST["title"]))>2)&&(strlen(test_input($_POST["title"])))<50){
		$title = test_input($_POST["title"]);	
		}else{
		$k=false;
		}
		if (is_numeric(test_input($_POST["year"]))&&(test_input($_POST["year"])<2017)&&(test_input($_POST["year"])>0)){
		$year = test_input($_POST["year"]);
		}else{
		$k=false;
		}
		if((strlen(test_input($_POST["author"]))>2)&&(strlen(test_input($_POST["author"])))<50){
		$author = test_input($_POST["author"]);	
		}else{
		$k=false;
		}
		if((strlen(test_input($_POST["genre"]))>2)&&(strlen(test_input($_POST["genre"])))<30){
		$genre = test_input($_POST["genre"]);	
		}else{
		$k=false;
		}
		if($k==false){
		echo "<p class='error'> blogai įvesti duomenys</p>";
		}
	}
}
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
$sql = "INSERT INTO books (title, year, author, genre) VALUES ('$title', '$year', '$author', '$genre')";
if(($title!='')&&($year!='')&&($author!='')&&($genre!='')){
         if (mysqli_query($conn, $sql)) {
        echo "<p class='success'>Duomenys sėkmingai išsaugoti</p>";
    } else {
        echo "Error: " . $sql . "<br>" . mysqli_error($conn);
 }
}
$title = $year = $author = $genre = "";	
/********************************************************/
$conn->close();
?>
			<table>
				<form  id="FormBook" name="FormBook" method="post" action="index.php"> 
				<tr>
				<td><label for="title">Knygos pavadinimas</label></td>
				<td><input type="text" class="form-control" name="title" id="title" placeholder=" nuo 3 iki 49 simbolių"></td>
				</tr>
				<tr>
				<td><label for="year">Leidimo metai</label></td>
				<td><input type="text" class="form-control"name="year" id="year" placeholder="1 - 2017"></td>
				</tr>
				<tr>
				<td><label for="author">Autorius(-iai)</label></td>
				<td><input type="text" class="form-control" name="author" id="author" placeholder=" nuo 3 iki 49 simbolių"></td>
				</tr>
				<tr>
				<td><label for="genre">Žanras</label></td>
				<td><input type="text" class="form-control" name="genre" id="genre" placeholder=" nuo 3 iki 29 simbolių"></td>
				</tr>
				<tr >
				<td colspan="2"><button type="submit" class="btn btn-default">Įvesti</button></td>
				</tr>
				</form>
			<table>
		</div>
		<div class="clear"></div>	
	</div>
</body>
</html>