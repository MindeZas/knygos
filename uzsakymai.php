<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Knygos</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="main-content">
		<div class="linkai">
			<ul>
				<li><a href="index.php">UŽSAKYMO FORMA </a></li>
				<li class="current" ><a href="uzsakymai.php"> UŽSAKYMAI </a></li>
			</ul>
		</div>
		<div class="container">
		<h3><b>KNYGŲ UŽSAKYMAI</b></h3>
	<?php
/***********************************************/
include('connection.inc.php');
/***********************************************/
/******************puslapaivimo init***********************/
	$per_page=10;
	if (isset($_GET["page"])) {
	$page = $_GET["page"];
	}
	else {
	$page=1;
	}
/********************************************************/
/*******************paieška ir peržiūra******************/
if(isset($_GET['search_query'])){
    $search_query = $_GET['search_query']; 
    $min_length = 2;
	$start_from = ($page-1) * $per_page;
    if(strlen($search_query) >= $min_length){ 
		$sql = "SELECT * FROM books   WHERE (`title` LIKE '%".$search_query."%') 
										OR (`author` LIKE '%".$search_query."%')
										OR (`year` LIKE '%".$search_query."%')
										OR (`genre` LIKE '%".$search_query."%')";
		$result = $conn->query($sql);
?>		
		<br>
		<a class="btn btn-default"  href="uzsakymai.php">  Grįžti į sąrašą  </a>
		<br>
<?php	echo "<p><b>..".$search_query."..</b> Paieškos rezultatai: </p>";
		if ($result->num_rows > 0){ 
			echo "<table class='table table-bordered'><th> Nr. </th><th> Pavadinimas </th><th> Metai </th><th> Autorius </th><th> Žanras </th>";	
			$i = 1;
			while($row = $result->fetch_assoc()){
				echo "<tr><td>".$i++."</td><td><a target='_blank' href=\"knyga.php?id=$row[id]\">" . $row["title"]. "</a></td><td>".$row["year"]."</td><td>".$row["author"]."</td><td>".$row["genre"]."</td></tr>";
			}
			echo "</table>";
		}else{
            echo "Nieko nerasta</br>";
		}
    }
    else{ 
        echo "turite įvesti mažiausiai ".$min_length."simbolus <br>";
		echo "<a class='btn btn-default'  href='uzsakymai.php'>  Grįžti į sąrašą  </a>";
    }
}else{
?>
	 <form class="form-inline col-md-offset-1" action="uzsakymai.php" method="GET"> 
		<label for="genre">Paieška</label>
		<input type="text" class="form-control" name="search_query" id="genre">
		<button type="submit" class="btn btn-default"> Ieškoti</button>
	</form>
	<table class="table"><th> Nr. </th><th> Pavadinimas </th><th> Metai </th><th> Autorius </th><th> Žanras </th>
	<tr><td>#</td><td>
	<form id="order_title_form" method="post">
	<select class="form-control" name = "order_title" onchange="change_title()">
		<option value="" selected >rūšiuoti</option>
		<option value="ASC">A -> Z</option>
		<option value="DESC">Z -> A</option>
	</select>
	</form>
	</td>
	<td>
	<form  id="order_year_form" method="post">
	<select  class="form-control" name = "order_year" onchange="change_year()">
		<option value="" selected >rūšiuoti</option>
		<option value="ASC">didėjančia tvarka</option>
		<option value="DESC">mažėjančia tvarka.</option>
	</select>
	</form>
	</td>
	<td>
	<form id="order_author_form" method="post">
	<select  class="form-control" name = "order_author" onchange="change_author()">
		<option value="" selected >rūšiuoti</option>
		<option value="ASC">A -> Z</option>
		<option value="DESC">Z -> A</option>
	</select>
	</form>
	</td>
	<td>
	<form id="order_genre_form" method="post">
	<select class="form-control" name = "order_genre" onchange="change_genre()">
		<option value="" selected >rūšiuoti</option>
		<option value="ASC">A -> Z</option>
		<option value="DESC">Z -> A</option>
	</select>
	</form>
		</td>
	</tr>
<?php	
	if(isset($_POST['order_title'])){
		$order_type = $_POST['order_title'];
		$order_name = "title";
	}else if(isset($_POST['order_year'])){
		$order_type = $_POST['order_year'];
		$order_name = "year";
	}else if(isset($_POST['order_author'])){
		$order_type = $_POST['order_author'];
		$order_name = "author";
	}else if(isset($_POST['order_genre'])){
		$order_type = $_POST['order_genre'];
		$order_name = "genre";
	}else{
	$order_name = "id";  
    $order_type = "ASC";   	
	}
	$start_from = ($page-1) * $per_page;
	$sql = "SELECT * FROM books ORDER BY {$order_name} {$order_type} LIMIT $start_from, $per_page";
	$result = $conn->query($sql);
	if ($result->num_rows > 0) {
		$i = 1;
		while($row = $result->fetch_assoc()) {
			echo "<tr><td>".$i++."</td><td><a target=\"_blank\" href=\"knyga.php?id=$row[id]\">" . $row["title"]. "</a></td><td>" . $row["year"]. "</td><td>" . $row["author"]. "</td><td>" . $row["genre"]."</td></tr>";
		}
	} else {
		echo "Knygų nerasta";
	}
	echo "</table>";
/********************puslapiavimo linkai***************************/
	$sql = "select * from books";
	$result = $conn->query($sql);
	$total_records = $result->num_rows;
	if($per_page < $total_records ){
		$total_pages = ceil($total_records / $per_page);
			echo "<div class='links'><a href=\"uzsakymai.php?page=1\">Pirm.psl.</a>&nbsp;&nbsp; ";
		for ($i=2; $i<=$total_pages-1; $i++) {
			echo "<a  href=\"uzsakymai.php?page=".$i."\">".$i."</a> ";	
		}
		echo "&nbsp;&nbsp;<a  href=\"uzsakymai.php?page=$total_pages\"> Pask.psl.</a></div>";
	}
}
/*********************************************************/
$conn->close();
?>
	</div><!---->
    <div class="clear"></div>	
</div>
<script>
function change_title(){
	document.getElementById("order_title_form").submit();
}
function change_year(){
    document.getElementById("order_year_form").submit();
}
function change_author(){
    document.getElementById("order_author_form").submit();
}
function change_genre(){
    document.getElementById("order_genre_form").submit();
}
</script>
</body>
</html>