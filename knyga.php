<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Knyga</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
</head>
<body>
<div class="row" style="margin-top:20px;">
<div class="col-md-8 col-md-offset-2">
<?php
/***********************************************/
include('connection.inc.php');
/***********************************************/

if(isset($_GET['id'])){
$id = (int)$_GET['id'];
$sql = "SELECT * FROM books WHERE id = $id";
$result = $conn->query($sql);

echo "<table class='table table-bordered'>";
	if($row = $result->fetch_assoc()) {
		echo "<th width='400' colspan='2'><h3>" . $row["title"]. "</h3></th>";
	    echo "<tr><td width='150'> Leidimo metai: </td><td>" . $row["year"]. "</td></tr>";
		echo "<tr><td width='150'> Autorius (-iai): </td><td>" . $row["author"]. "</td></tr>";
		echo "<tr><td width='150'> Žanras: </td><td>" . $row["genre"]. "</td></tr>";
	}else{
		echo "Tokia knyga nerasta";
	}
}else{
	echo "It is not set.";
}
echo "</table>";
$conn->close();
?>
</div>
</div>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>