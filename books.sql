-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2018 at 10:54 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `books_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `genre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `year`, `author`, `genre`) VALUES
(74, 'KelionÄ— Ä¯ nakties pakraÅ¡tÄ¯', 1932, 'Luisas-Ferdinandas Selinas', 'romanas'),
(70, 'Svetimas', 1942, 'Alberas Kamiu', 'romanas'),
(73, 'MaÅ¾asis princas', 1943, 'Antuanas de Sent Egziuperi', 'pasaka'),
(72, 'Procesas', 1925, 'Francas Kafka', 'romanas'),
(71, 'Prarasto laiko beieÅ¡kant', 1927, 'Marselis Prustas', 'romanas'),
(76, 'Kam skambina varpai', 1940, 'Ernestas HemingvÄ—jus', 'romanas'),
(77, 'Le Grand Meaulnes', 1913, 'Alain-Fournier', 'Bildungsromanas'),
(78, 'DienÅ³ puta', 1947, 'Borisas Vianas', 'romanas'),
(79, 'Antroji lytis', 1949, 'Simona de Bovuar', 'filosofija'),
(80, 'Belaukiant Godo', 1952, 'Samuelis Beketas', 'drama'),
(82, 'RoÅ¾Ä—s vardas', 1980, 'Umberto Eko', 'romanas'),
(83, 'Gulago archipelagas', 1973, 'Aleksandras SolÅ¾enicynas', 'romanas'),
(84, 'Paroles', 1946, 'Jacques PrÃ©vert', 'romanas'),
(85, 'Alcools', 1913, 'Guillaume Apollinaire', 'poezija'),
(86, 'Tenteno nuotykiai. MÄ—lynasis Lotosas', 1936, 'Georges RÃ©mi', 'komiksai'),
(87, 'DienoraÅ¡tis', 1947, 'Ana Frank', 'Autobiografija'),
(88, 'Tristes tropiques', 1955, 'Klodas Levi-Strosas', 'romanas'),
(89, 'Puikus naujas pasaulis', 1932, 'Oldosas Hakslis', 'romanas'),
(91, 'Asteriksas iÅ¡ Galijos', 1959, 'RenÃ© Goscinny ir Albert Uderzo', 'komiksai'),
(92, 'La Cantatrice chauve', 1952, 'EugÃ¨ne Ionesco', 'pjese'),
(93, 'Three Essays on the Theory of Sexuality', 1905, 'Zigmundas Froidas', 'psichologija'),
(94, 'Lolita', 1955, 'Vladimiras Nabokovas', 'romanas'),
(95, 'Ulisas', 1922, 'DÅ¾eimsas DÅ¾oisas', 'romanas'),
(96, 'Il deserto dei Tartari', 1940, 'Dino Buzzati', 'romanas'),
(97, 'PinigÅ³ padirbinÄ—tojai', 1925, 'AndrÄ— Å½idas', 'romanas'),
(98, 'Le Hussard sur le toit', 1951, 'Jean Giono', 'romanas'),
(99, 'Belle du Seigneur', 1968, 'Albert Cohen', 'romanas'),
(101, 'TriukÅ¡mas ir Ä¯nirÅ¡is', 1929, 'Viljamas Folkneris', 'romanas'),
(102, 'TerezÄ— Deskeiru', 1927, 'Fransua Moriakas', 'romanas'),
(103, 'Zazie dans le mÃ©tro', 1959, 'Raymond Queneau', 'romanas'),
(104, 'Verwirrung der GefÃ¼hle', 1927, 'Stefanas Cveigas', 'novelÄ—'),
(105, 'VÄ—jo nublokÅ¡ti', 1936, 'Margaret MitÄel', 'romanas'),
(106, 'Ledi ÄŒaterli meiluÅ¾is', 1928, 'Deividas Herbertas Lorensas', 'romanas'),
(107, 'UÅ¾burtas kalnas', 1924, 'Tomas Manas', 'romanas'),
(108, 'Sveikas liÅ«desy', 1954, 'Fransuaza Sagan', 'romanas'),
(109, 'Le Silence de la mer', 1942, 'Jean Bruller', 'drama'),
(111, 'BaskerviliÅ³ Å¡uo', 1902, 'ArtÅ«ras Konan Doilis', 'romanas'),
(112, 'Under the Sun of Satan', 1926, 'Georges Bernanos', 'romanas'),
(113, 'Didysis Gatsbis', 1925, 'Frencis Skotas FidÅ¾eraldas', 'romanas'),
(114, 'PokÅ¡tas', 1967, 'Milan Kundera', 'satyra'),
(115, 'Il disprezzo', 1954, 'Alberto Moravia', 'novelÄ—'),
(116, 'RodÅ¾erio Ekroido nuÅ¾udymas', 1926, 'Agata Kristi', 'Detektyvas'),
(117, 'NadÅ¾a', 1928, 'AndrÄ— Bretonas', 'poezija'),
(118, 'AurÄ—lien', 1944, 'Louis Aragon', 'romanas'),
(119, 'Le Soulier de satin', 1929, 'Paul Claudel', 'romanas'),
(120, 'Alchemikas', 1990, 'Paul Coel', 'romanas');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
